-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: vs1-prod-20200819-production-vpc.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_domotz_iot_devices`
--

DROP TABLE IF EXISTS `tbl_domotz_iot_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_domotz_iot_devices` (
  `iot_device_id` bigint NOT NULL,
  `company_id` bigint NOT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `agent_id` bigint NOT NULL,
  `status` enum('OFFLINE','ONLINE','DOWN','HIDDEN') DEFAULT NULL,
  `paused` tinyint DEFAULT '0',
  `health` int DEFAULT '0',
  `health_info` json DEFAULT NULL,
  `uptime` datetime DEFAULT NULL,
  `importance` enum('VITAL','FLOATING') DEFAULT NULL,
  `type` int DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint DEFAULT '1',
  `notes` varchar(300) DEFAULT NULL,
  `make` varchar(100) DEFAULT NULL,
  `is_monitored` tinyint DEFAULT '0',
  `is_agent_monitored` tinyint DEFAULT '1',
  `group_type` enum('AV_DEVICES','NETWORK_DEVICES','MOBILE_DEVICES','OFFICE_DEVICES','SMART_DEVICES','SERVERS','OTHER_DEVICES') DEFAULT 'OTHER_DEVICES',
  `live_stats` json DEFAULT NULL,
  `event_data` json DEFAULT NULL,
  `other_data` json DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_type` enum('video','audio','cloud') DEFAULT NULL,
  `device_id` int DEFAULT NULL,
  `device_site_id` int unsigned DEFAULT NULL,
  `is_manually_linked` tinyint DEFAULT '0',
  PRIMARY KEY (`iot_device_id`),
  KEY `tbl_domotz_iot_devices_device_site_id_foreign` (`device_site_id`),
  CONSTRAINT `tbl_domotz_iot_devices_device_site_id_foreign` FOREIGN KEY (`device_site_id`) REFERENCES `tbl_site` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_domotz_iot_devices`
--

LOCK TABLES `tbl_domotz_iot_devices` WRITE;
/*!40000 ALTER TABLE `tbl_domotz_iot_devices` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_domotz_iot_devices` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-08 23:15:25
