-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: vs1-prod-20200819-production-vpc.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_desktop_plugin_versions`
--

DROP TABLE IF EXISTS `tbl_desktop_plugin_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_desktop_plugin_versions` (
  `desktop_plugin_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`desktop_plugin_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_desktop_plugin_versions`
--

LOCK TABLES `tbl_desktop_plugin_versions` WRITE;
/*!40000 ALTER TABLE `tbl_desktop_plugin_versions` DISABLE KEYS */;
INSERT INTO `tbl_desktop_plugin_versions` VALUES (1,'1.0.14.0','VisibilityOneDesktopPluginSetup-1.0.14.0.exe','2021-02-01 05:26:47',0,'21MB','Initial version',NULL,NULL,'COMPLETED',0),(2,'1.0.18.0','VisibilityOneDesktopPluginSetup_1.0.18.0.exe','2021-02-11 23:47:32',0,'21MB','token plugin link',NULL,NULL,'COMPLETED',0),(3,'1.0.19.0','VisibilityOneDesktopPluginSetup_1.0.19.0.exe','2021-03-24 23:32:56',0,'22MB','Fix for cpu info',NULL,NULL,'COMPLETED',0),(4,'1.0.21.0','VisibilityOneDesktopPluginSetup_1.0.21.0.exe','2021-04-09 02:40:00',0,'22MB','Modified ping and new plugin data for teams',NULL,'2021-04-09 17:00:00','COMPLETED',0),(5,'1.0.22.0','VisibilityOneDesktopPluginSetup_1.0.22.0.exe','2021-04-09 07:31:20',0,'22MB','Call simulator fixes',NULL,'2021-04-20 02:11:00','COMPLETED',0),(6,'1.0.24.0','VisibilityOneDesktopPluginSetup_1.0.24.0.exe','2021-04-24 06:51:58',0,'22MB','Added silent installer and system default device inuse',NULL,NULL,'COMPLETED',0),(7,'1.0.25.0','VisibilityOneDesktopPluginSetup_1.0.25.0.exe','2021-05-03 22:24:13',0,'22MB','CPU usage optimization',NULL,'2021-05-05 00:00:00','COMPLETED',0),(8,'1.0.27.0','VisibilityOneDesktopPluginSetup_1.0.27.0.exe','2021-06-16 07:50:26',0,'22MB','removed teams presence in data sending and added Save logs label in zip button',NULL,NULL,'COMPLETED',0),(9,'1.0.28.0','VisibilityOneDesktopPluginSetup_1.0.28.0.exe','2021-07-01 00:27:46',0,'22MB','Fix: in use values not showing on first teams/zoom call',NULL,NULL,'COMPLETED',0),(10,'1.0.29.0','VisibilityOneDesktopPluginSetup_1.0.29.0.exe','2021-07-13 22:27:19',0,'22MB','Fix issue of pathping in specific environment for teams',NULL,NULL,'COMPLETED',0),(11,'1.0.30.0','VisibilityOneDesktopPluginSetup_1.0.30.0.exe','2021-07-15 22:25:38',0,'22MB','Fix issue of get ip list in specific environment for zoom',NULL,NULL,'COMPLETED',0),(12,'1.0.32.0','VisibilityOneDesktopPluginSetup_1.0.32.0.exe','2022-03-09 21:27:05',0,'22MB','Changed behavior in call app simulator',NULL,NULL,'COMPLETED',0),(13,'1.0.33.0','VisibilityOneDesktopPluginSetup_1.0.33.0.exe','2022-03-14 10:51:02',0,'22MB','fixed empty device in windows 11',NULL,NULL,'COMPLETED',0),(14,'1.0.36.0','VisibilityOneDesktopPluginSetup_1.0.36.0.exe','2022-04-07 11:31:35',0,'22MB','Fixed disconnection issue due to cpu info error in WMI',NULL,'2023-01-11 13:28:00','COMPLETED',0),(15,'1.0.38.0','VisibilityOneDesktopPluginSetup_1.0.38.0.exe','2023-02-17 00:00:00',0,'23.8MB','Added windows 11 support',NULL,NULL,'COMPLETED',0),(16,'2.0.0.1','VisibilityOneDesktopPluginSetup_2.0.0.1.exe','2023-05-19 00:00:00',1,'23.8MB','Auto update support',NULL,NULL,'OPEN',0);
/*!40000 ALTER TABLE `tbl_desktop_plugin_versions` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-08 23:17:16
