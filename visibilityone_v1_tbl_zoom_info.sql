-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: vs1-prod-20200819-production-vpc.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_zoom_info`
--

DROP TABLE IF EXISTS `tbl_zoom_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_zoom_info` (
  `zoom_id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `APIKey` varchar(255) DEFAULT NULL,
  `APISeceret` varchar(255) DEFAULT NULL,
  `Token` varchar(255) DEFAULT NULL,
  `active` int DEFAULT '1',
  `last_error_stamp` datetime DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `notification` int DEFAULT '1',
  `web_hook_token_verification` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`zoom_id`),
  KEY `idx_company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zoom_info`
--

LOCK TABLES `tbl_zoom_info` WRITE;
/*!40000 ALTER TABLE `tbl_zoom_info` DISABLE KEYS */;
INSERT INTO `tbl_zoom_info` VALUES (32,444,'thR2mVQwS4q2W9PK9LKOBA','ZODVCvfFp3LmG1ccGQttzzi4rj3e5ImF',NULL,1,NULL,'',1,'wlU0TDH_TYy_8NcMTnQWeA'),(49,36,'OD2pAr_NTWamwel7ztKQVQ','mxSZ2dBSgP9o6RfB9Y0pjpg4KRPNfeyq',NULL,1,NULL,NULL,1,'UAyC2GL9SH6b6go5jGSUug'),(50,433,'p3mwphrhS5iDYM56UdSUQ','QmBre7rMvybp2b0U0E4nMcj6V5z5erpF',NULL,1,NULL,'',1,'pooU4ueMRmiAWKR554B-Qw'),(51,552,'qMk4MnONSCWw8BhPoouGg','OKPQDCPQ6fF52d8hw5UQcqQvQLtIlnna',NULL,1,NULL,NULL,0,'f_EQszDaTiKv5GKnd_HJrw'),(52,34,'F8NN4Go_SRCsrQ0M133nNQ','yAfSPRp1Qtex5fLjC0n2sG7gvSLvDNaY',NULL,1,NULL,NULL,1,'L5pslvNGQe2qqNwY74wR6g'),(53,548,'*********','**********',NULL,1,NULL,'',0,'**********');
/*!40000 ALTER TABLE `tbl_zoom_info` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-08 23:17:46
